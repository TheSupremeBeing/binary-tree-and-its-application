#include "bet.h"
#include <iostream>
#include <cctype>
#include <cstdlib>
#include <string>
#include <vector>
#include <stack>
#include <sstream>

// Helper Functions // 
bool isOperator(string s) 
{
    return s == "/" || s == "*" || s == "+" || s == "-";
}

// Public Functions // 

// default zero-parameter constructor.
BET::BET()
{
    root = NULL;
}

// one-parameter constructor, where parameter "postfix" is 
// string containing a postfix expression. The tree should 
// be built based on the postfix expression. Tokens in the 
// postfix expression are separated by space.
BET::BET(const string postfix)
{
    root = NULL;
    if(!buildFromPostfix(postfix)) makeEmpty(root);
}

// copy constructor
BET::BET(const BET& orig)
{
    root = NULL;
    *this = orig;
}

// destructor
BET::~BET()
{
    makeEmpty(root);
}

// parameter "postfix" is string containing a postfix 
// expression. The tree should be built based on the 
// postfix expression. Tokens in the postfix expression 
// are separated by space. If the tree contains nodes 
// before the function is called, you need to first 
// delete the existing nodes. Return true if the new tree 
// is built successfully. Return false if an error is 
// encountered.
bool BET::buildFromPostfix(const string postfix) // DONE
{ 
    // make sure tree is empty
    makeEmpty(root);

    // get "tokens" of postfix string
    vector<string> v;
    stringstream ss(postfix);
    while (!ss.eof())
    {
        string token;
        getline(ss, token, ' ');
        v.push_back(token);
    }

    // tokens exists
    stack<BinaryNode *> s;
    if (v.size() > 0)
    {
        // single token must be operand
        if (v.size() == 1 && !isOperator(v[0]))
        {
            root = new BinaryNode;
            root->element = v[0];
            root->left = NULL;
            root->right = NULL;
            return true;
        }

        // make tree
        for (string t : v)
        {
            if(!isOperator(t))
            {
                BinaryNode * a = new BinaryNode;
                a->element = t;
                a->left = NULL;
                a->right = NULL;
                s.push(a);
            } 
            else if (s.size() >= 2) // operator found
            {
                root = new BinaryNode;
                root->element = t;
                root->right = s.top();
                s.pop();
                root->left = s.top();
                s.pop();
                s.push(root);
            }
        }
    }
    // stack should contain root only
    return s.size() == 1;
}

// assignment operator
const BET & BET::operator= (const BET & lhs)
{
    makeEmpty(root);
    root = clone(lhs.root);
}

// call the private version of the printInfixExpression 
// function to print out the infix expression.
void BET::printInfixExpression()
{
    if(!empty())
        printInfixExpression(root);
    cout << endl;
}

// call the private version of the printPostfixExpression
// function to print out the postfix expression.
void BET::printPostfixExpression()
{
    if(!empty())
        printPostfixExpression(root);
    cout << endl;
}

// call the  private version of the size function to 
// return the number of nodes in the tree
size_t BET::size()
{
    return size(root) - 1;
}

// call the private version of the leaf_nodes function 
// to return the number of leaf nodes in the tree.
size_t BET::leaf_nodes()
{
    return leaf_nodes(root);
}

// return true if the tree is empty. Return false otherwise.
bool BET::empty()
{
    return root == NULL;
}

// Private Fucntions // 

// print to the standard output the corresponding infix 
// expression. Note that you may need to add parentheses 
// depending on the precedence of operators. You should not 
// have unnecessary parentheses.
void BET::printInfixExpression(BinaryNode *n)
{
    if(n != NULL)
    {   
        if (isOperator(n->element))
        {
            cout << "( ";
        }
        printInfixExpression(n->left);
        cout << n->element << " ";
        printInfixExpression(n->right);
        if (isOperator(n->element))
        {
            cout << ") ";
        }
    }
}

// print to the standard output the corresponding postfix 
// expression.
void BET::printPostfixExpression(BinaryNode *n)
{
    if(n != NULL)
    {   
        printPostfixExpression(n->left);
        printPostfixExpression(n->right);
        cout << n->element << " ";
    }
}

// delete all nodes in the subtree pointed to by t. Called by
// functions such as the destructor.
void BET::makeEmpty(BinaryNode* &t)
{
    if(t == NULL) return;

    makeEmpty(t->left);
    makeEmpty(t->right);
    delete t;
}

// clone all nodes in the subtree pointed to by t. Called by
// functions such as the assignment operator=.
BET::BinaryNode * BET::clone(BinaryNode *t) const
{
    if(t == NULL) return NULL;

    BinaryNode * b = new BinaryNode();
    b->element = t->element;
    b->left = clone(t->left);
    b->right = clone(t->right);
    
    return b;
}

// return the number of nodes in the subtree pointed to by t.
size_t BET::size(BinaryNode *t)
{
    if (t == NULL)
        return 1;
    else
        return size(t->left) + size(t->right);
}

// return the number of leaf nodes in the subtree pointed to by t.
size_t BET::leaf_nodes(BinaryNode *t)
{
    if (t == NULL)
        return 0;
    if (t->left == NULL && t->right == NULL)
        return 1;
    else
        return leaf_nodes(t->left) + leaf_nodes(t->right);
}
