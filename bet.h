#ifndef _BET_H_
#define _BET_H_

#include <string>
using namespace std;

class BET
{
    struct BinaryNode
    {
        string element;
        BinaryNode * left;
        BinaryNode * right;
    };
    public:
        BET();
        BET(const string postfix);
        BET(const BET&);
        ~BET();
        bool buildFromPostfix(const string postfix);
        const BET & operator= (const BET &);
        void printInfixExpression();
        void printPostfixExpression();
        size_t size();
        size_t leaf_nodes();
        bool empty();

    private:
        BinaryNode *root;

        void printInfixExpression(BinaryNode *n);
        void makeEmpty(BinaryNode* &t);
        BinaryNode * clone(BinaryNode *t) const;
        void printPostfixExpression(BinaryNode *n);
        size_t size(BinaryNode *t);
        size_t leaf_nodes(BinaryNode *t);
    };

#include "bet.cpp"
#endif