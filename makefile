proj4.x: proj4_driver.cpp bet.cpp
	g++47 proj4_driver.cpp -o proj4.x -std=c++11
	
run: proj4.x
	./proj4.x

test: proj4.x
	./proj4.x < tests.txt
	
clean:
	rm proj4.x

tar: bet.cpp bet.h
	tar -cvf proj4.tar bet.h bet.cpp analysis.txt makefile
